# Làm sao để nâng cấp và phát triển sách hiếm

Chào cô Xuân,

Cháu biết đến trang sachhiem.net cách đây hơn sáu năm, khi cố giáo sư Trần Chung Ngọc vẫn còn sinh thời. Rồi khi giáo sư Ngọc tạ thế, trong lòng cháu luôn trăn trở một nỗi lo rằng, làm sao để tiếp tục duy trì và phát triển sách hiếm khi những người như giáo sư Ngọc một ngày nào đó cũng sẽ khuất núi, khi mà tri thức, trí tuệ vẫn chưa kịp truyền đạt hết cho lớp hậu sinh.

Gần đây khi đọc [thông báo](https://www.facebook.com/sachhiem.net.us/posts/876254286044536) của giáo sư Nguyễn Mạnh Quang về tình trạng khó khăn về nhân lực, vật lực, tài chính để xuất bản và công bố tài liệu về sự can thiệp của Vatican vào Việt Nam.

Thì sự lo lắng và nỗi trăn trở càng lớn hơn. Cháu tự nhủ mình phải làm gì đó để tiếp sức thực hiện [sứ mệnh và lý tưởng](https://sachhiem.net/EMAILS/SH/Sachhiem28.php) mà những người sáng lập sách hiếm theo đuổi.

Vì lý đó, này cháu viết thư này gửi đến cô trình bày một số vấn đề sau:

### 1. Vấn đề nhân sự cho sách hiếm.

Theo thông tin mà	Ban quản trị công bố, thì hiện tại có 3 trụ cột chính điều hành sách hiếm là cô `Lý Thái Xuân` sáng lập viên. Kỹ thuật viên là cô `Nguyễn Thị Phúc` và giáo sư `Nguyễn Mạnh Quang` làm cố vấn lịch sử. Gần đây theo một số thông tin thì cháu được biết cô `Xuân` và cô `Phúc` là một, tức là cô vừa đảm nhận vai trò biên tập vừa phụ trách kỹ thuật và duy trì hoạt động website.

Cháu không rõ ngoài hai thành viên trụ cột trên còn có thêm nhân sự làm công tác quản trị, biên tập và xuất bản hay không. 

Vì đây là công việc tốn nhiều thời gian và rất vất vả mà chỉ có ban quản trị đảm trách thì khó mà đòi hỏi cao về chất lượng nội dung bài viết lẫn hình thức trình bày sao cho chuyên nghiệp, cuốn hút.

Kết quả là ta đã đóng cửa trang `sách hiếm nói` dù nội dung rất hay, trang `sachhiem.org` và `sachhiem.net` đều có hạn chế và hình thức trình bày khó theo dõi, khó tìm kiếm, khó sử dụng trên các thiết bị di động, không thân thiện với các cỗ máy tìm kiếm như google. Chung quy lại là khó tiếp cận được đông đảo bạo đọc dù nhiều bài viết có nội dung và tư liệu rất giá trị trình bày rất công phu.

Do đó cần thiết lập cơ chế sao cho có sự tham gia và chia sẽ công việc từ cộng đồng, những người có cùng chung lý tưởng. Nhằm nâng cao chất lượng nội dung lẫn hình thức trình bày sao cho khả năng tiếp cận bạn đọc là tốt nhất.

### 2. Thiết lập cơ chế có sư tham gia của cộng đồng.

Có nhiều công cụ hỗ trợ chúng ta làm việc này như:

- **gitlab**: là một công cụ quản lý mã nguồn phần mềm, nhưng có thể tận dụng để quản trị bài viết với sự tham gia của nhiều người. Thế mạnh của công cụ này là giúp theo dấu, truy vết và xem lại lịch sử thay đổi, cập nhật, chỉnh sửa bài viết được lưu trữ vĩnh viễn mà các công cụ soạn thảo offline thông thường như Microsoft Word không thể làm được.
- **stackeditpro**: đây là công cụ hỗ trợ viết bài và lưu trữ trực tiếp lên `gitlab`. Người viết chỉ tập trung vào việc sáng tạo nội dung, còn phần biên tập, sửa lỗi chính tả, chỉnh sửa, xuất bản, lưu trữ có thể chia sẽ cho nhiều người cùng làm. Và quan trọng nhất là gần như sẽ không bị tình trạng phá hoại như hacker tấn công, virus hay bị kiểm duyệt, xóa bài, mất bài do sự cố hay mất điện và tất cả nội dung sẽ được đồng bộ và lưu tự động vào `gitlab`
- **bookstack**: là nền tảng dùng xuất bản sách, chúng ta hoàn toàn có thể tự xuất bản nếu đủ kinh phí duy trì host
- **slack**: đây là công cụ chat, trao đổi làm việc nhóm hoạt động tương tự như messenger của facebook nhưng thân thiện, nhiều tính năng và chuyên nghiệp hơn. Đặc biệt quan trọng nhất là không bị tình trạng kiểm duyệt, khóa, xóa tài khoản đảm bảo luôn hoạt động thông suốt.

Trên là bốn công cụ chính miễn phí mà giới lập trình viên thường sử dụng để làm việc nhóm online cực kỳ hiệu quả, với sự tham gia của các thành viên từ bất kỳ đâu trên thế giới, miễn máy tính có kết nối internet. Chúng ta có thể tận dụng các công cụ này vào việc quản trị và điều hành sách hiếm một cách tiện lợi và hiệu quả với chi phí tiết kiệm nhất.

### 3. Xây dựng website mới
Từ những bất cập của hệ thống website cũ, ta nên có kế hoạch xây dựng website mới, website mới phải thỏa yêu cầu như: thân thiện với người dùng, trình bày đẹp, cuốn hút, dễ điều hướng, tra cứu, tìm kiếm, hỗ trợ các thiết bị di động như điện thoại, máy tính bảng, cho phép người dùng bình luận.

Đây là website mẫu, cho trang sách hiếm mới

https://reditory.gbjsolution.com/

### 4. Tài chính 
Theo cháu biết các tình nguyện viên cộng tác với sách hiếm(nếu có) đều không có thù lao. Kinh phí hoạt động của sách hiếm hiện nay do ban biên tập tự bỏ tiền túi duy trì hoạt động.

Nếu thực hiện kế hoạch xây dựng website mới thì kinh phí chủ yếu là thuê máy chủ vps vì website mới xây dựng trên mã nguồn ghost.org nên không hoạt động trên môi trường share host thông tường mà cần phải có vps. Kinh phí khoảng 10USD/tháng, tức khoảng 120USD/năm.

https://www.digitalocean.com/pricing

Trước mắt ban quản trị sách hiếm có thể tài trợ chi phí này, về lâu dài khi website mới hoàn chỉnh ta có thể chạy quảng cáo để hỗ trợ kinh phí hoạt động và kêu gọi cộng đồng quyên góp.

### 5. Kế hoạch triển khai
Cháu không rõ hiện nay sách hiếm được sao lưu dự phòng ở đâu, nhưng để an toàn ta nên lưu trữ trên gitlab. Đầu tiên ta sẽ chuyển các bài viết mang tính tư liệu lịch sử, có giá trị sử liệu cao lên gitlab trước sau đó đến các bài viết khác. 

Các bài viết trước khi chuyển sẽ được biên tập, chỉnh sửa, cập nhật thông tin trước khi lưu trữ trên gitlab. Sau khi website mới hoàn thành ta sẽ chuyển đăng lại các bài viết này lên website mới.

Các bài viết là rất nhiều, nên ta sẽ kêu gọi cộng đồng hỗ trợ, cô Châu phụ trách khâu biên tập, giáo sư `Nguyễn Manh Quang` chỉ làm công tác thẩm định các nội dung chỉnh sửa cuối cùng.

Việc xây dựng website mới sẽ thực hiện song song với việc lưu trữ bài viết từ sách hiếm lên gitlab.

### 6. Cháu có thể hỗ trợ được gì
Là một lập trình viên phần mềm làm việc nhiều năm cháu sẽ phụ trách phần kỹ thuật lập trình và xây dựng website mới, phối hợp với nhân sự kỹ thuật của sách hiếm giải quyết các vấn đề về kỹ thuật, quản trị. Đồng thời hỗ trợ, chuyển giao tất cả các vấn đề kỹ thuật cho nhân sự sách hiếm.

Khi công việc vận hành đã đi vào ổn định cháu sẽ dần rút ra và giao lại toàn quyền quản trị cho sách hiếm.

Tham gia viết bài, đề xuất, đóng góp ý kiến xây dựng sách hiếm đúng với lý tưởng và sứ mệnh của sáng lập viên.

Tất cả công việc tham gia trên cơ sở tự nguyện và hoàn toàn không thù lao hay đòi hỏi bất kỳ quyền lợi nào.

### 7. Sứ mệnh của Sách Hiếm trong tình hình mới

Trong một tranh luận mới đây trên facebook, nhiều người tỏ ra bức xúc và bất mãn vì Chính phủ Việt Nam tăng cường bang giao với Vatican với mục đích là kiềm chế sự hung hăng và manh động của đám con chiens trong nước trong lúc Việt Nam phải đang vất vả tìm giải pháp đối phó với hành vi dòm ngó và xâm lấn chủ quyền của Trung Quốc.

Vậy làm thế nào để cùng lúc đối phó với ngoại xâm và nội thù. Trong binh pháp việc đối đầu cùng lúc với nhiều kẻ thù là điều tối kỵ. Do đó để không bị rơi và kịch bản tồi tệ trên và có thể dành toàn lực đối phó với Trung Quốc, thì phải xử lý triệt để đám nội thù tay sai vatican bên trong.

#### Có 2 giải pháp:

##### 1. Giải pháp bàn tay sắt:

Thực hiện chính sách bàn tay sắt nhổ cỏ tận gốc, giệt trừ hậu họa sau này như cách nước Nhật từng làm với đám Kito bản địa. Nếu thực hiện chính sách  này ta phải trả lời câu hỏi rằng bao nhiêu % dân số Việt Nam sẵn sàng "ăn cỏ" như Triều Tiên trước thủ đoạn bao vây cấm vận của Mỹ và phương Tây?

FB Anh Tuấn Lê cho rằng trước đây thời TBT như Đô Mười, Lê Khả Phiêu không ai đi "chầu" Vatican cả, sao bây giờ cả bốn vị lãnh đạo quốc gia đều phải qua bang giao với Vatican.

Để trả lời vấn đề trên cần bối cảnh lịch sử của Việt Nam trong mỗi giai đoạn. Thời TBT Đỗ Mười, Lê Khả Phiêu Việt Nam không có mấy cái hiệp định kinh tế như WTO, CPTPP hay EVFTA, thời đó Việt Nam cũng không tham gia hội đồng nhân quyền LHQ, không là ủy viên không thường trực LHQ.

Việt Nam thời đó cũng không có tăng trưởng kinh tế 6-7%, cũng không xuất siêu vào Mỹ như hiện nay mà người dân phải ăn cơm độn bo bo, bị Mỹ và phương Tây bao vây cấm vận.

Đây là cái giá phải trả khi tham gia vào cuộc chơi lớn do đám thực dân, đế quốc Mỹ và phương Tây cầm đầu.

Sẽ có bao nhiêu % dân số Việt Nam sẵn sàng quay lại Việt Nam thời TBT Đỗ Mười, Lê Khả Phiêu?

Nói vậy không có nghĩa là chúng ta để đám giặc Kito và đám quan thầy của chúng là gì thì làm. Vấn đề là làm thế này tận dụng nguồn lực quốc tế để xây dụng nội lực và hạn chế thấp nhất tác hại của chúng.

##### 2. Giải pháp rút củi đáy nồi: 
Như phân tích trên, chúng ta ai cũng biết thế lực kito giáo và vatican ở Việt Nam bản chất là những con ngựa thành Troy, là những quả bom nổ chậm chỉ chờ thời cơ thuận lợi là thừa lệnh Vatican nổi dậy làm loạn. 

Lịch sử đã chứng minh con chiens đi đến đâu là thế lực Vatican bành trướng đến đó. Số lương con chiens càng đông, thì thế lực Vatican càng lớn. Do đó sứ mạng của SachHiem là phải làm sao ngăn chặn thủ đoạn cải đạo của Vatican và tiến thới giải phóng chon chiens khỏi sự chi phối và điều khiển từ tà giáo và thần quyền Vatican.

Chiến lược này tam chia thành 2 giai đoạn, giai đoạn đầu là làm sao hạn chế thấp nhất khả năng người dân bị cải đạo, giai đoạn hai là khiến con chiens nhận ra sự thật mà tự giác bỏ đạo.

Đây chỉ là cách chia tạm thời để xác định trọng tâm chiến lược, đánh giá cái nào ưu tiên trước cái nào sau, để các các hoạt động cụ thể sao cho phù hợp và hiệu quả nhất chứ không nhất thiết phải rạch ròi, vì trong quá trình thực hiện có thể kết hợp bổ sung qua lại.

Trên là một số đề xuất và giải pháp nâng cao chất lượng cũng như hình thức `sachhiem` nhắm tăng khả năng tiếp cận với đông đảo người dân Việt Nam.

Mong nhận được phản hồi của Cô.

Cuối thư chúc cô và giáo sư Quang năm mới nhiều sức khỏe, an lành.






<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc3NjE3MDM4OSwtMTI1MDEwNTA3LC0xMT
I2MzIzMjUxLC05NTI0NTM4NiwtMTQ5NTU0NzY1NiwtMjUyMjQz
ODAxLDE4Njg5MzkxNzAsLTEzMDYyMTczMDEsMjEzMzE0OTg5OC
wtMjc1NzcwODkxLDE4NTc2NzkyNTMsMTAzNDIwOTQwLDE2NTgz
MjA1NzQsMTE3OTA0NjExNiwxODI4NzY2NjY2LC0xMDc3NjYwOT
A4LDE3NDI3OTQ1NTIsMTIyODE1NDAzMCw1ODg1MzUxMTddfQ==

-->