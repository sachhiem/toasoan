ĐẢ PHẬT BÀI TRUNG TRONG SÁCH LƯƠC KITO HÓA VIỆT NAM

Lịch sử đã chứng minh con chiens đi đến đâu là thế lực Vatican bành trướng đến đó. Điều này giải thích tại sao trong các hiệp ước Việt Nam ký kết với Âu - Mỹ, một trong những yêu sách của chúng là "tự do tôn giáo" tức là cho phép tôn giáo mà trọng tâm là kito giáo được tự do cải đạo mà không chịu sự kiểm soát, giới hạn của Nhà nước. 

Về bản chất đây là một cách nói khác của việc yêu cầu Việt Nam để chúng tư do tuyển mộ và huấn luyện tay sai, gián điệp bản địa một cách công khai dưới cái tên mị dân là tự do tôn giáo.

Về lý thuyết đây là hình thức vô hiệu hóa quyền lực nhà nước đối với hoạt động tuyển mộ và huấn luyện lực lượng tay sai, bao gồm cả thành phần làm nhiệm vụ chỉ điểm, gián điệp,  xây dựng lực lượng và phá hoại các công trình, chính sách trọng điểm quốc gia của các thế lực thực dân, đế quốc và đầu sỏ chính trị quốc tế bằng thủ đoạn tôn giáo. 

Nguy hiểm hơn ngày nay chúng đang đẩy mạnh việc công khai đưa kito giáo vào trường học, vào các cơ quan tuyền thông, y tế thậm chí là các hoạt động văn hóa thể thao để quá trình kito hóa Việt Nam diễn ra nhanh hơn.

Khi mà quyền lực nhà nước đã bị vô hiệu hóa thì nền tảng văn hóa truyền thống mỗi gia đình ở Việt Nam trở thành phòng tuyến cuối cùng trước đại họa Kito. 

Kẻ thù biết rất rõ điều này nên chúng đang tăng cường phá hoại truyền thống văn hóa gia đình Việt Nam với trọng tâm là tách giới trẻ ra khỏi ảnh hưởng truyền thống gia đình, tiêm nhiễm vào đầu chúng những giá trị lai căng, vọng ngoại đồng thời thực hiện thủ đoạn phá hoại có chiến lược các giá trị văn hóa, tín ngưỡng, tôn giáo truyền thống trong điểm quốc gia. 

Điều này giải thích tại sao trong những năm gần đây các chiến dịch kêu gọi bỏ tết cổ truyền, chuyển sang tết tây diễn ra rầm trên khắp các mặt trận truyền thông. Thập chí chúng còn trắng trợn ra yêu sách đề nghị thừa nhận ngày lễ Kito thành ngày nghỉ lễ quốc gia, trong khi ngày tri ân các anh hùng liệt sỹ 27/07 thì chúng chống phá quyết liệt.

Chưa dừng lại ở đó, Phật giáo một trong những thành cuối cùng nơi lưu giữ các giá trị nhân văn, văn hóa truyền thống cũng chịu chung số phận, trỏ thành mục tiêu chống phá không khoan nhượng. 

Ngày xx sau hàng tháng "ăn cơm cửa phật" nằm phục kích tại chùa Ba Vàng, đám lưu manh ở cái động đĩ tên báo lao động bắt đầu làm cái chuyện thất đức là "đốt râu thầy chùa" bằng các loạt bài phóng sự đấu tố chùa Ba Vàng.

Các chiến dịch đánh phá phật giáo diễn ra rầm rộ trên truyền thông thậm chí vào cả quốc hội là bằng chứng đanh thép không thể chối cãi. 

Bằng cách chụp cho cái mũ kinh doanh chùa, kinh doanh tâm linh, hay BOT chùa chúng đã tạo ra một cơ hội không thể tốt hơn để tự do đánh phá phật giáo một cách công khai, quyết liệt mà không sợ bị lên án.

Nham hiểm hơn trong khi đánh phá phật giáo chúng không từ thủ đoạn đẩy phật giáo vào thế đối đầu với các tín ngưỡng bản địa khác.

https://tuoitre.vn/ba-pham-thi-yen-long-ngon-ve-tin-nguong-tho-mau-20190325195706512.htm

Một thủ đoạn tạo mâu thuẫn, gây chia rẽ và làm suy yếu các tôn giáo, tín ngưỡng bản địa cực kỳ tinh vi và nham hiểm.

Chụp mũ kinh doanh Chùa để đánh vào uy tín, tạo mẫu thuẫn đối kháng với các tín ngưỡng bản địa để làm suy yếu Phật giáo. Nhưng đó chưa phải là tất cả! 

Ngày 08/04/2018 cái động đĩ mang tên vnexpress ra đòn kết liễu bằng bài viết cực dài, được đầu tư công phu cả đồ họa lẫn nội dung mang tên "Công đức chuyện một dòng tiền không kiểm toán"

https://vnexpress.net/longform/cong-duc-chuyen-mot-dong-tien-khong-kiem-toan-3905326.html

Toàn bộ bài viết là thủ đoạn dẫn dắt đám đông một cách bài bản và tinh vi để cuối cùng kết bài chúng ghim vào đầu đám đông cái tâm lý bài xích và nghi ngờ về số tiền công đức ở các ngôi chùa.

> Sau vụ tham ô tiền công đức, những người bị đem ra xét xử đều đã đi tù
> vài năm rồi về. Khu di tích cũng đã có ban quản lý mới. Người dân như
> bà Xuyên khi được hỏi đều đã để câu chuyện vào quá vãng. Nhưng cũng kể
> từ đấy, bà ít khi lên Đền. Bà Xuyên bảo, do bận việc ruộng đồng, rồi
> kết lại một câu bâng quơ, "bây giờ chả tin được ai nữa".

Và chốt bài là lời kêu gọi ngừng lên chùa, ngừng hỗ trợ tài chính cho chùa.

> Đó có thể là một lựa chọn tốt, theo quan điểm của bà Xuyên. Bởi chỉ có
> thế, bà mới không phải bận tâm nữa đến số phận của những cái hòm. Và
> cả những câu chuyện sau lúc mở hòm.

Một thủ đoạn phải nói là cực kỳ nham hiểm và thâm độc. Bởi nó đi đến tận cùng của sự xảo quyệt, mất dạy và khốn nạn nhưng vẫn che đậy được thủ đoạn trước đám đông bằng cái bản mặt sạch sẽ, trí thức.

Dù đánh phá Phât giáo một cách nham hiểm và thâm độc như vậy, nhưng chúng tỏ ra mù lòa trước những việc làm sai trái, thách thức pháp luật của đám giặc Kito. 

- Nguyễn Đình Thục Đặng Hữu Nam, biểu tình, lấn chiếm đất, ép con chiens bỏ học
- Nguyễn Ngọc Nam Phong khiêu khích chính quyền 
- Nguyễn Ngọc Thanh, Nguyễn Duy Tân vinh danh ngụy

Những chỉ dấu này cho thấy, kẻ thù đã chơi bài ngữa, sẵn sàng đánh, phủ đầu vỗ mặt bất kỳ cá nhân, tổ chức nào, thậm chí cả chính quyền mà chúng không vừa ý hay có mâu thuẫn lợi ích với chúng.

Nhà thờ đá đề xuất thu phí
https://vnexpress.net/thoi-su/nha-tho-da-nha-trang-muon-thu-phi-khach-nuoc-ngoai-3985527.html
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwODU1MTY1NjUsLTEwOTA0NDM3MjldfQ
==
-->